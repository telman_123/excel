﻿using CompareExcels;

var file1 = FileHandler.ShowDialog();
var file2 = FileHandler.ShowDialog();

var file1Data = ExcelReader.LoadFirstColumn(file1);
var file2Data = ExcelReader.LoadFirstColumn(file2);
var matchCount = file2Data.Count(file1Data.Contains);

var match1 = Math.Round((double)matchCount / file1Data.Count * 100, 2);
var match2 = Math.Round((double)matchCount / file2Data.Count * 100, 2);


Console.WriteLine($"File1: {file1Data.Count} elements");
Console.WriteLine($"File2: {file2Data.Count} elements");
Console.WriteLine($"Matched: {matchCount} elements - {match1}% and {match2}%");
