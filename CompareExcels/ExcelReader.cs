﻿using ClosedXML.Excel;

namespace CompareExcels;

public static class ExcelReader
{
    public static HashSet<string> LoadFirstColumn(string path, bool convertToMD5 = false)
    {
        var result = new HashSet<string>();
        var workbook = new XLWorkbook(path);
        var worksheet = workbook.Worksheet(1);
        var count = worksheet.RowsUsed().Count();
        for (int i = 1; i <= count; i++)
        {
            var originalString = worksheet.Cell(i, 1).GetString();

            if (originalString.StartsWith("0"))
                originalString = originalString[1..];

            if (!originalString.StartsWith("374"))
                originalString = "374" + originalString;

            if (!originalString.StartsWith("+"))
                originalString = "+" + originalString;

            result.Add(originalString);
        }

        return result;
    }
}
