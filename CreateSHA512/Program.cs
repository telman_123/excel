﻿using ClosedXML.Excel;
using CreateSHA512;
using System.Security.Cryptography;

WriteObjectInToExcelAsync(FileOpen.ShowDialog());
static void WriteObjectInToExcelAsync(string fileName)
{
    var workbook = new XLWorkbook(fileName);
    var worksheet = workbook.Worksheet(1);
    var count = worksheet.RowsUsed().Count();

    var rowCount = 1;
    for (int i = 1; i <= count; i++)
    {
        string originalString = worksheet.Cell(i, 1).GetString();

        if (originalString.StartsWith("0"))
        {
            originalString = "374" + originalString[1..];
        }

        if (originalString.StartsWith("+374"))
        {
            originalString = "374" + originalString[4..];
        }

        if (!originalString.StartsWith("374"))
            originalString = "374" + originalString;
        if (!string.IsNullOrEmpty(originalString))
        {
            SHA512 sha512 = SHA512.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(originalString);
            byte[] hashBytes = sha512.ComputeHash(inputBytes);
            string hashedString = BitConverter.ToString(hashBytes).Replace("-", "");

            worksheet.Cell(i, 1 + 1).Value = hashedString;
            Console.WriteLine(rowCount);
            rowCount++;
        }
    }
    workbook.SaveAs(fileName);
}